FROM debian:9
RUN apt -y update && apt -y install wget build-essential autoconf automake pkg-config libtool libpcre++-dev libssl-dev zlib1g-dev gcc
RUN ls
RUN mkdir nginx

RUN wget http://nginx.org/download/nginx-1.18.0.tar.gz && tar xvzf nginx-1.18.0.tar.gz && pwd
RUN wget  https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz && tar xvzf v2.1-20200102.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && tar xvzf v0.3.1.tar.gz && mv ngx_devel_kit-0.3.1  ngx_devel_kit
RUN wget  https://github.com/openresty/lua-nginx-module/archive/v0.10.15.tar.gz  && tar xvzf v0.10.15.tar.gz && ls

WORKDIR /luajit2-2.1-20200102
RUN make 
RUN make install

WORKDIR  /nginx-1.18.0
ENV LUAJIT_LIB=/usr/local/lib   
ENV LUAJIT_INC=/usr/local/include/luajit-2.1
RUN ./configure --prefix=/opt/nginx  --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-module=/ngx_devel_kit --add-module=/lua-nginx-module-0.10.15
RUN make
RUN make install
#COPY ./nginx.conf /opt/nginx/conf/nginx.conf

FROM debian:9
COPY --from=0 /opt /opt
COPY --from=0 /usr/local/lib /usr/local/lib
RUN chmod +x /opt/nginx/sbin/nginx

EXPOSE 80
CMD ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]
